#!/bin/bash
# Author: Milos Buncic
# Date: 2019/05/23
# Description: Build psycopg2 python module inside of docker container with statically linked libpq

set -e

# Use environment variables otherwise fallback to default values
PSYCOPG_VERSION=${PSYCOPG_VERSION:-"2.8.4"}
POSTGRESQL_VERSION=${POSTGRESQL_VERSION:-"9.6.9"}
PYTHON_VERSION=${PYTHON_VERSION:-"3.7"}
SSL_ENABLED=${SSL_ENABLED:-"true"}

DOCKER_BUILD_IMAGE_NAME="lambci/lambda:build-python${PYTHON_VERSION}"
WORKING_DIR=${WORKING_DIR:-"/var/task"}

run() {
  # Run docker container based on lambci/lambda docker image
  # https://github.com/lambci/docker-lambda
  docker run -it --rm --name psycopg2-build \
    -w "${WORKING_DIR}" \
    -v "${PWD}":"${WORKING_DIR}" \
    -e PSYCOPG_VERSION \
    -e POSTGRESQL_VERSION \
    -e PYTHON_VERSION \
    -e SSL_ENABLED \
    ${DOCKER_BUILD_IMAGE_NAME} ./$(basename ${0}) build
}

build() {
  # Mutate string 2.8.x to 2-8
  local PSYCOPG_VERSION_TMP1=(${PSYCOPG_VERSION//./ })
  local PSYCOPG_VERSION_TMP2=${PSYCOPG_VERSION_TMP1[@]:0:2}
  local PSYCOPG_VERSION_MM=${PSYCOPG_VERSION_TMP2/ /-}

  # Cleanup
  rm -rvf psycopg2

  # Download and extract source code files for postgresql (libpq) and psycopg python module
  curl -sSL -O https://ftp.postgresql.org/pub/source/v${POSTGRESQL_VERSION}/postgresql-${POSTGRESQL_VERSION}.tar.gz
  curl -sSL -O http://initd.org/psycopg/tarballs/PSYCOPG-${PSYCOPG_VERSION_MM}/psycopg2-${PSYCOPG_VERSION}.tar.gz
  tar xzvf postgresql-${POSTGRESQL_VERSION}.tar.gz
  tar xzvf psycopg2-${PSYCOPG_VERSION}.tar.gz

  # Compile and install postgresql (libpq)
  cd ${WORKING_DIR}/postgresql-${POSTGRESQL_VERSION}
  ./configure --with-openssl --without-readline --without-zlib
  make && make install

  # Compile pg_config
  cd src/bin/pg_config
  make

  # Reconfigure and compile psycopg2 python module (statically linked libpq)
  cd ${WORKING_DIR}/psycopg2-${PSYCOPG_VERSION}
  sed -i "s%pg_config =%pg_config = ${WORKING_DIR}/postgresql-${POSTGRESQL_VERSION}/src/bin/pg_config/pg_config%" setup.cfg
  sed -i "s%static_libpq = 0%static_libpq = 1%" setup.cfg
  if [[ ${SSL_ENABLED} == "true" ]]; then
    sed -i "s%have_ssl = 0%have_ssl = 1%" setup.cfg
    sed -i "s%libraries =%libraries = ssl crypto%" setup.cfg
  fi
  python${PYTHON_VERSION} setup.py build

  # Export compiled psycopg2 python module
  mv build/lib.linux-x86_64-*/psycopg2 "${WORKING_DIR}"

  # Cleanup
  rm -rvf "${WORKING_DIR}"/{psycopg2-*,postgresql-*}
}

case ${1} in
  'build')
    build
  ;;
  *)
    run
esac
