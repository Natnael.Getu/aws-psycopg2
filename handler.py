import psycopg2

def pgtest(event, context):
  """
  Test PostgreSQL connection
  """
  conn = psycopg2.connect(**event['connect'])
  cur = conn.cursor()
  cur.execute(event['query'])

  payload = str(cur.fetchall())

  cur.close()
  conn.close()

  return payload
