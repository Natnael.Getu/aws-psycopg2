aws-psycopg2
============

About
-----

Build `psycopg2` python module inside of docker container with statically linked `libpq`

Build
-----

Modify environment variables to properly configure module before build time

```bash
export PSYCOPG_VERSION="2.8.4"
export POSTGRESQL_VERSION="9.6.9"
export PYTHON_VERSION="3.7"
export SSL_ENABLED="true"
```

Build `psycopg2`

```palin
./build.sh
```

Test
----

It is not mandatory but if you want you can also run local or remote tests with simple [pgtest](./handler.py) lambda to make sure that module is working as expected.

### Local

Run local PostgreSQL server inside of container

```plain
docker run -d --rm --name postgres -e POSTGRES_USER=test -e POSTGRES_PASSWORD=password postgres
```

Invoke `pgtest` function

**Note:** `sslmode` is set to `disable`

```plain
docker run -it --rm --name pgtest -v "${PWD}":/var/task lambci/lambda:python3.7 handler.pgtest '{"connect": {"host": "'$(docker inspect -f '{{.NetworkSettings.Networks.bridge.IPAddress}}' postgres)'", "port": 5432, "user": "test", "password": "password", "dbname": "test", "sslmode": "disable"}, "query": "SELECT * FROM pg_stat_activity;"}'
```

Remove PostgreSQL local container

```plain
docker rm -f postgres
```

### Remote (Lambda)

The [Serverless Framework](https://serverless.com/framework/docs/providers/aws/guide/installation/) helps you develop and deploy your AWS Lambda functions, along with the AWS infrastructure resources they require.
It's a CLI that offers structure, automation and best practices out-of-the-box, allowing you to focus on building sophisticated,
event-driven, serverless architectures, comprised of Functions and Events.

#### Deploy

Before deployment modify security group and subnet ids for your VPC in the [serverless.yml](./serverless.yml) configuration file

```yaml
vpc:
  securityGroupIds:
    - sg-018d45d5b52c7b612
  subnetIds:
    - subnet-0b6de16c4fc30f322
    - subnet-069ef5458869b0ab7
```

Deploy `pgtest` lambda

```plain
sls deploy -v
```

#### Invoke

Example test event

**Note:** Valid `sslmode` values are `disable`, `allow`, `prefer`, `require`, `verify-ca`, `verify-full`

```json
{
  "connect": {
    "host": "10.0.0.10",
    "port": 5432,
    "user": "test",
    "password": "password",
    "dbname": "test",
    "sslmode": "require"
  },
  "query": "SELECT * FROM pg_stat_activity;"
}
```

Invoke `pgtest` function

```plain
sls invoke -f pgtest -d '{"connect": {"host": "10.0.0.10", "port": 5432, "user": "test", "password": "password", "dbname": "test", "sslmode": "require"}, "query": "SELECT * FROM pg_stat_activity;"}'
```

#### Remove

Remove whole stack

```plain
sls remove -v
```

Prebuilt modules
----------------

### With SSL

- [psycopg2-2.8.2-python2.7-libpq9.6.9.tar.gz](https://gitlab.com/psyhomb/aws-psycopg2/-/jobs/218987682/artifacts/raw/target/psycopg2-2.8.2-python2.7-libpq9.6.9.tar.gz)
- [psycopg2-2.8.2-python3.7-libpq9.6.9.tar.gz](https://gitlab.com/psyhomb/aws-psycopg2/-/jobs/218987884/artifacts/raw/target/psycopg2-2.8.2-python3.7-libpq9.6.9.tar.gz)
- [psycopg2-2.8.3-python3.7-libpq9.6.9.tar.gz](https://gitlab.com/psyhomb/aws-psycopg2/-/jobs/274229181/artifacts/raw/target/psycopg2-2.8.3-python3.7-libpq9.6.9.tar.gz)
- [psycopg2-2.8.4-python3.7-libpq9.6.9.tar.gz](https://gitlab.com/psyhomb/aws-psycopg2/-/jobs/359638181/artifacts/raw/target/psycopg2-2.8.4-python3.7-libpq9.6.9.tar.gz)
